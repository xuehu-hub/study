package com.study.design.create.single;

/**
 * @author baogx
 * @date 2020年9月23日
 */
public class HungrySingleton {
    private static final HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {}

    public static HungrySingleton getInstance() {
        return instance;
    }
}
