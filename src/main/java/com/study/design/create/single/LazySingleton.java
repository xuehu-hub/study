package com.study.design.create.single;

/**
 * @author baogx
 * @date 2020年9月23日
 * 懒汉式
 */
public class LazySingleton {
    /** 如果要保证线程安全 使用volatile synchronized 的关键字，但会影响性能 */
    private static volatile LazySingleton instance = null;

    private LazySingleton() {}

    public static synchronized LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }

}
